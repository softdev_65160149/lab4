/* * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license 
* Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template 
*/ 
package com.mycompany.lab4;

import java.util.Scanner;

/**
 * * * @author pattarapon
 */
public class Game {

    private Table table;
    private Player player1, player2;
    static Scanner kb = new Scanner(System.in);

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void Play() {
        showWelcome();
        newGame();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            if (table.checkWin()) {
                saveWin();
                showWin();
                showInfo();
                if (checkContinue()) {
                    newGame();
                    table.switchPlayer();
                } else {
                    break;
                }
            }
            if (table.checkDraw()) {
                saveDraw();
                showTable();
                System.out.println("Tie on one win");
                showInfo();
                if (checkContinue()) {
                    newGame();
                    table.switchPlayer();
                } else {
                    break;
                }
            }
            table.switchPlayer();
        }
    }

    private void showWelcome() {
        System.out.println("welcome to xo game");
    }

    private void showTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println();
        }
    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    public void inputRowCol() {
        System.out.print("please input row col : ");
        int row = kb.nextInt();
        int col = kb.nextInt();
        if (table.getTable()[row - 1][col - 1] != '-') {
            System.out.println("can't move");
            inputRowCol();
        } else {
            table.setRowCol(row - 1, col - 1);
        }
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " turn");
    }

    private void showWin() {
        showTable();
        System.out.println(table.getCurrentPlayer().getSymbol() + " win");
    }

    private boolean checkContinue() {
        System.out.print("continue or Exit(y/n) : ");
        String play = kb.next();
        return play.equals("y");
    }

    public void saveWin() {
        if (player1 == table.getCurrentPlayer()) {
            player1.win();
            player2.lose();
        } else {
            player2.win();
            player1.lose();
        }
    }

    public void saveDraw() {
        player1.draw();
        player2.draw();
    }

    private void showInfo() {
        System.out.println(player1);
        System.out.println(player2);
    }
}
